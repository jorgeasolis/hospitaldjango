#!/bin/bash
set -e
set -o pipefail

ip_pre="3.143.230.26"
dns_pre="ec2-3-143-230-26.us-east-2.compute.amazonaws.com"

#repo="HospitalDjango"
repo="$gitlabSourceRepoName"

> ~/.ssh/known_hosts
ssh-keyscan  -H $dns_pre >> ~/.ssh/known_hosts
ssh-keyscan  -H $ip_pre >> ~/.ssh/known_hosts

cd ..
scp -r -i ~ubuntu/llave_aws.pem $repo ubuntu@$dns_pre:/tmp/

ejecutar="$(cat <<-EOF
sudo mysqldump hospital2 > hospital2.sql
sudo apt-get update; apt-get install apache2 mariadb-server 
sudo cp /tmp/$repo/hospital.conf /etc/apache2/sites-available/
virtualenv env_hospital
. env_hospital/bin/activate
pip install -r /tmp/$repo/requirements.txt
sudo rm -rf $repo
mv /tmp/$repo .
sudo systemctl restart apache2.service
rm $repo/{deploy.sh,Jenkinsfile,hospital.conf,script_jenkins.sh}
EOF
)"

ssh -i ~ubuntu/llave_aws.pem ubuntu@$dns_pre "$ejecutar"