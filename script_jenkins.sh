#!/bin/bash
set -e
set -o pipefail


echo "Cosas de construcción y más"
pwd
whoami
cd ..
virtualenv env_hospital
. env_hospital/bin/activate
cd -
pip install -r requirements.txt
python manage.py collectstatic --noinput